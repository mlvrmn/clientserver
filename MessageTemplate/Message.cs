﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageTemplate
{
    [Serializable]
    public class Message
    {
        public String Text { get; set; }
        public DateTime DateTime{get; set;}

        public Message(String text, DateTime dateTime)
        {
            this.Text = text;
            this.DateTime = dateTime;
        }

        public Message()
        {
            
        }

    }
}
