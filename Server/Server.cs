﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using MessageTemplate;

namespace ServerNameSpace
{
    public class Server
    {
        private XmlSerializer _xmlSerializer;
        private Message _receivedMessage;
        public Server()
        {
            _xmlSerializer = new XmlSerializer(typeof(Message));
        }

        public void ToReceiveData(String _exchangeFile)
        {
            XmlReader xmlReader = XmlReader.Create(_exchangeFile);
            _receivedMessage = (Message)_xmlSerializer.Deserialize(xmlReader);
            xmlReader.Close();
        }

        public void ToSendData(String _exchangeFile)
        {
            Message sendMessage = new Message();

            switch (_receivedMessage.Text)
            {
                case "Версия ОС":
                    sendMessage.Text = Environment.OSVersion.VersionString;
                    break;

               
                case "Имя компьютера":
                    sendMessage.Text = Environment.MachineName;
                    break;


                case "Количество процессоров":
                    sendMessage.Text = Environment.ProcessorCount.ToString();
                    break;

                case "Логические диски":
                    sendMessage.Text = String.Join(",", Environment.GetLogicalDrives());
                    break;

                default:
                    sendMessage.Text = "Неверная команда";
                    break;
            }


            sendMessage.DateTime = DateTime.Now;

            XmlWriter xmlWriter = XmlWriter.Create(_exchangeFile);
            _xmlSerializer.Serialize(xmlWriter, sendMessage, null);
            xmlWriter.Close();
        }
    }
}
