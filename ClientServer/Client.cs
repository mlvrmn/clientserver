﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using MessageTemplate;

namespace ClientServer
{
    public class Client 
    {
        private XmlSerializer _xmlSerializer;
        public Client()
        {
            _xmlSerializer = new XmlSerializer(typeof(Message));
        }
         
        public void ToReceiveData(String _exchangeFile)
        {
            Message receivedMessage;
            XmlReader xmlReader = XmlReader.Create(_exchangeFile);
            receivedMessage = (Message)_xmlSerializer.Deserialize(xmlReader);
            xmlReader.Close();

            Console.WriteLine(receivedMessage.Text);
            Console.WriteLine();
        }

        public void ToSendData(String _exchangeFile)
        {
            String data = Console.ReadLine();
            DateTime dateTime = DateTime.Now;
            Message message = new Message(data, dateTime);

            XmlWriter xmlWriter = XmlWriter.Create(_exchangeFile);
            _xmlSerializer.Serialize(xmlWriter, message, null);
            xmlWriter.Close();
        }
    }
}
