﻿using System;
using System.IO;
using ServerNameSpace;
using System.Timers;


namespace ClientServer
{
    public class Connector
    {
        private Client _client;
        private Server _server;
        private String _exchangeFile;
        private Timer  _timer;

        public Connector(Client client, Server server, String exchangeFile)
        {
            _client = client;
            _server = server;
            _exchangeFile = exchangeFile;
            _timer = new Timer(3000);
        }

        public void StartExchange()

        {
            if (!File.Exists(_exchangeFile))
            {
                Console.WriteLine("Ошибка. Файл для обмена не существует.");
                Console.WriteLine("Файл будет создан системой автоматически.");

                File.Create(_exchangeFile);
            }

            _timer.Elapsed += ElapsedTime;
            _timer.AutoReset = false;

            while (true)
            {
                _client.ToSendData(_exchangeFile);

                _server.ToReceiveData(_exchangeFile);

                Console.WriteLine("Данные обрабатываются сервером, подождите.......");
                _timer.Start();
            }
        }

        private void ElapsedTime(Object sender, ElapsedEventArgs e)
        {
            _server.ToSendData(_exchangeFile);
            _client.ToReceiveData(_exchangeFile);
        }



    }
}
