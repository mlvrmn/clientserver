﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerNameSpace;

namespace ClientServer
{
    class Program
    {
        static void Main(string[] args)
        {
            String exchangeFile = @"D:\FileExchange.xml";
            Client client = new Client();
            Server server = new Server();
            Connector connector = new Connector(client, server, exchangeFile);


            Console.WriteLine("Введите одну из следующих команд :\n" +
                "Версия ОС\n" +
                "Имя компьютера\n" +
                "Количество процессоров\n" +
                "Логические диски\n"
                );


            connector.StartExchange();
        }
    }
}
